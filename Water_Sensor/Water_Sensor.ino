#define Water_Sensor A0
#define LED 9

void setup() {
  // put your setup code here, to run once:
  pinMode(Water_Sensor, INPUT);
  pinMode(LED, OUTPUT); 

  Serial.begin(9600); 
}

void loop() {
  // put your main code here, to run repeatedly:
  int rd = analogRead(Water_Sensor);
  Serial.println(rd); 

  if(rd <= 130){
    digitalWrite(LED, HIGH); 
  } else {
    digitalWrite(LED, LOW); 
  }
} 
