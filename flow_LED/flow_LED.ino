const int lowPin = 2; 
const int highPin = 9; 

void setup() { 
  for (int thisPin = lowPin; thisPin <= highPin; thisPin++){
    pinMode(thisPin, OUTPUT); 
  }
}

void loop() { 
  for(int thisPin = lowPin; thisPin<=highPin; thisPin++){
    digitalWrite(thisPin,HIGH); 
    delay(100); 
  }

  for(int thisPin = highPin; thisPin >=lowPin; thisPin--){
    digitalWrite(thisPin,LOW); 
    delay(100); 
  }
}

