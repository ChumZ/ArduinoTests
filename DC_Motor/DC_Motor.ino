const int M1 = 9;
const int M2 = 10; 

void setup() {
  // put your setup code here, to run once:
  pinMode(M1,OUTPUT); 
  pinMode(M2,OUTPUT); 
}

void loop() {
  // put your main code here, to run repeatedly:
  clockwise(200);
  delay(1000); 
  cclockwise(200);
  delay(1000);
}

void clockwise(int Speed){ 
  analogWrite(M1,Speed);
  analogWrite(M2,0);
} 

void cclockwise(int Speed){ 
  analogWrite(M1,0);
  analogWrite(M2,Speed); 
} 

