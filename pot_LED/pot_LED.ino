const int ledPin = 9;
const int analogPin = A0; 
int in = 0;
int out = 0; 

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); 
} 

void loop() {
  // put your main code here, to run repeatedly:
  in = analogRead(analogPin); 
  out = map(in,0,1023,0,255);
  analogWrite(ledPin,out); 
  Serial.println(in); 
  Serial.println(out); 
}
