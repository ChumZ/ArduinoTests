//Variable Decleration
const int red = 11;
const int green = 10; 
const int blue = 9; 

void setup() {
  // put your setup code here, to run once:
  pinMode(red,OUTPUT);
  pinMode(green,OUTPUT);
  pinMode(blue,OUTPUT); 
}

void loop() {
  // put your main code here, to run repeatedly:
  color(255,0,0); 
  delay(1000); 
  color(0,255,0);
  delay(1000);
  color(0,0,255);
  delay(1000); 
}

//FUNCTION DECLERATION
void color (unsigned char redp, unsigned char greenp, unsigned char bluep){ 
  analogWrite(red,redp);
  analogWrite(green,greenp); 
  analogWrite(blue,bluep); 
}

